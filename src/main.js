import Vue from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'
import GlobalPlugin from './utils/global-plugin'

Vue.config.productionTip = false
Vue.use(GlobalPlugin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// eslint-disable-next-line
import '@/assets/sass/element_variables.scss'
