
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const Media = () => import('./containers/Media.vue')

export default {
  path: '/media',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Media',
      components: { default: Media, header: DefaultHeader }
    }
  ]
}
