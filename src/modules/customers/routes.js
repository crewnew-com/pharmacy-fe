
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const Customers = () => import('./containers/Customers.vue')

export default {
  path: '/customers',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Customers',
      components: { default: Customers, header: DefaultHeader }
    }
  ]
}
