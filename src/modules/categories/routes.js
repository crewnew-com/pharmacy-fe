
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const Categories = () => import('./containers/Categories.vue')

export default {
  path: '/categories',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Categories',
      components: { default: Categories, header: DefaultHeader }
    }
  ]
}
