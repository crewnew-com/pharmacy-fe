
import HomeLayout from '@/layouts/HomeLayout.vue'
import { DefaultHeader } from '@/shares'

const Home = () => import('./containers/Home.vue')

export default {
  path: '/',
  component: HomeLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Home',
      components: { default: Home, header: DefaultHeader }
    }
  ]
}
