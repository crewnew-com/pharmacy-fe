
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const LatestOrders = () => import('./containers/LatestOrders.vue')

export default {
  path: '/orders',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Latest Orders',
      components: { default: LatestOrders, header: DefaultHeader }
    }
  ]
}
