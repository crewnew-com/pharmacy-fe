
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const Others = () => import('./containers/Others.vue')

export default {
  path: '/other-link',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '',
      name: 'Other Link',
      components: { default: Others, header: DefaultHeader }
    }
  ]
}
