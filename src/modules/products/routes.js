
import DashboardLayout from '@/layouts/DashboardLayout.vue'
import { DefaultHeader } from '@/shares'

const Products = () => import('./containers/Products.vue')

export default {
  path: '/products',
  component: DashboardLayout,
  beforeEnter (to, from, next) {
    next()
  },
  children: [
    {
      path: '/',
      name: 'Products',
      components: { default: Products, header: DefaultHeader }
    }
  ]
}
