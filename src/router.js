import Vue from 'vue'
import Router from 'vue-router'

import { routes as Home } from '@/modules/home'
import { routes as Orders } from '@/modules/orders'
import { routes as Customers } from '@/modules/customers'
import { routes as Categories } from '@/modules/categories'
import { routes as Media } from '@/modules/media'
import { routes as Products } from '@/modules/products'
import { routes as Others } from '@/modules/others'

import NotFound from '@/modules/NotFoundPage.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      component: NotFound
    }
  ],
  linkActiveClass: 'active'
})

router.addRoutes([
  Home,
  Orders,
  Customers,
  Categories,
  Media,
  Products,
  Others
])

export default router
