import { fgInput, DropDown, Card, Button } from '@/components'
import { Input, InputNumber, Tooltip, Popover } from 'element-ui'

const GlobalComponents = {
  install (Vue) {
    Vue.component('fg-input', fgInput)
    Vue.component('drop-down', DropDown)
    Vue.component('card', Card)
    Vue.component('n-button', Button)
    Vue.component(Input.name, Input)
    Vue.component(InputNumber.name, InputNumber)
    Vue.use(Tooltip)
    Vue.use(Popover)
  }
}

export default GlobalComponents
