import 'es6-promise/auto'
import './polyfills'

import { Notifications, SideBar } from '@/shares'
import VeeValidate from 'vee-validate'
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import 'bootstrap/scss/bootstrap.scss'
import '@/assets/sass/now-ui-dashboard.scss'
import '@/assets/css/demo.css'

locale.use(lang)

export default {
  install (Vue) {
    Vue.use(GlobalComponents)
    Vue.use(GlobalDirectives)
    Vue.use(SideBar)
    Vue.use(Notifications)
    Vue.use(VeeValidate, { fieldsBagName: 'veeFields' })
  }
}
